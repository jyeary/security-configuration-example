/*
 * Copyright 2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bluelotussoftware.example;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
@SessionScoped
public class IndexBean implements Serializable {

    private static final long serialVersionUID = -442832517882750116L;
    private static final String COOKIE_NAME = "NSA-JSESSIONID";

    private String message;

    public IndexBean() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String invalidate() {
        FacesContext fc = FacesContext.getCurrentInstance();
        ExternalContext ec = fc.getExternalContext();
        Map<String, Object> cookieMap = ec.getRequestCookieMap();

        if (cookieMap.containsKey(COOKIE_NAME)) {
            Cookie c = (Cookie) cookieMap.get(COOKIE_NAME);
            c.setMaxAge(0);
            System.out.println(MessageFormat.format("setMaxAge(0) on {0} : {1}", COOKIE_NAME, c.getValue()));
        }

        ec.invalidateSession();
        return null;
    }
}
